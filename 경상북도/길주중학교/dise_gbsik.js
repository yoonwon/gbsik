var request = require("request"); 
var cheerio = require('cheerio');
var fs = require('fs');

//텍스트가 생성되는 기본 위치
var path = "C:\\DISEContent\\disehm\\";
//텍스트안에 이미지 파일 기본 파일 패스
var pathImg = "C:\\DISEContent\\disehm\\img\\";

var domain = "http://school.gyo6.net";

var gbsikurl = domain + "/gilju/81006/food"; //길주중학교

var defauleImg = "/images/addon/food/bg_diet_mn.png";

var MORNING = 1; 
var LAUNCH = 2;
var DINNER = 3;

function updateFile(type, contents, imgsrc) {
    
    var mealPath = path + "today_meal_" + type + ".txt";
    var mealImgPath = path + "today_meal_img_" + type + ".txt";
    var mealImg = pathImg + "today_meal_img_" + type + ".jpg";

    fs.writeFileSync(mealPath, contents);
    console.log('File Write Done  : ' + type);

    download(imgsrc, mealImg, function(){
        console.log('downloaded');
        fs.writeFileSync(mealImgPath, mealImg);
    });
}

var download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};


var requestOptions = { method: "GET", uri: gbsikurl, headers: { "User-Agent": "Mozilla/5.0" }};

function requestGbsik() {
    request(requestOptions, function(error, response, body) { 
        // 웹사이트 로드
        $ = cheerio.load(body); 
        
        today_list = []; 
        var index_day = 0;

        let dataSchema = {
            menu: '',
            allergyInfo: ''
        }

        // 미세먼지 예보 정보를 가지고있는 첫번째 테이블을 확인 
        $('.diet_tb').first().find('.today').each(function (index, elem) { 

                $(this).find('li').each(function (index, elem) {

                    var menu = $(this).find('.menu');

                    var newBody = cheerio.load(menu.html().replace(/<br>/gi, '\n'));
                    dataSchema.menu = newBody.text();
                    dataSchema.menu = dataSchema.menu.replace(/\d+|\.|\*/g, '');
                    dataSchema.menu = dataSchema.menu.replace(/B/gi, '');

                    console.log("today : " + index + " " + dataSchema.menu); 


                    var iamgeObj = $(this).find('img').attr('src');
                    var imageURL;

                    if (iamgeObj !== null && iamgeObj !== undefined) {
                        imageURL = domain + iamgeObj;
                    } else {
                        imageURL = domain + defauleImg;
                    }

                    updateFile(index, dataSchema.menu, imageURL);

                    console.log("today : " + index + " " + imageURL);
            });
        });         
    }); 
}

requestGbsik();

setInterval(function(){
    requestGbsik()
}, 10 * 60 * 1000);
                        
