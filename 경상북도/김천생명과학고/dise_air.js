var request = require("request"); 
var cheerio = require('cheerio');
var fs = require('fs');

var path = "C:\\DISEContent\\disehm\\";
var pathImg = "C:\\DISEContent\\disehm\\img\\";

var domain = "https://www.airkorea.or.kr/web/";
var airurl = domain + "vicinityStation?item_code=10008&station_code=437131"; //김천시

function updateAirFile(airContents, airKorean, imgContents) {
    
    var airPath = path + "today_air" + ".txt";
    var airTextPath = path + "today_air_korean" + ".txt";
    var airImgPath = path + "today_air_img" + ".txt";

    var airImgLinkPath = pathImg + imgContents + ".png";


    fs.writeFileSync(airPath, airContents);
    fs.writeFileSync(airImgPath, airImgLinkPath);
    fs.writeFileSync(airTextPath, airKorean);

    console.log('File Write Done  : ' + airPath);
}


var requestOptions = { method: "GET", uri: airurl, headers: { "User-Agent": "Mozilla/5.0" }};

function airCondition(airContents) {


    var conditions = "air_good";

    if (airContents <= 15) {
        conditions = "air_good";
    } else if (airContents <= 35) {
        conditions =  "air_not_good";
    } else  if (airContents <= 75) {
        conditions = "air_bad";
    } else {
        conditions = "air_worst";
    }

    return conditions;
}

function airConditionKorean(airContents) {


    var conditions = "좋아요";

    if (airContents <= 15) {
        conditions = "좋아요";
    } else if (airContents <= 35) {
        conditions =  "보통이에요";
    } else  if (airContents <= 75) {
        conditions = "나빠요";
    } else {
        conditions = "외출자제해요";
    }

    return conditions;
}

function requestAir() {
    request(requestOptions, function(error, response, body) { 
        
        $ = cheerio.load(body); 
        
        var airContents = $("table").find('td').first().text();
        airContents = airContents.replace(/㎍\/㎥/gi, '');
        console.log(airContents);

        var airContition = airCondition(airContents);
        var airKorean = airConditionKorean(airContents);

        updateAirFile(airContents, airKorean, airContition);

    }); 
}

requestAir();

setInterval(function(){
    requestAir()
}, 10 * 60 * 1000);
                        
