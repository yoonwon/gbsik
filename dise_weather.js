var request = require("request"); 
var convert = require('xml-js');
var fs = require('fs');

var path = "C:\\DISEContent\\disehm\\";
var pathImg = "C:\\DISEContent\\disehm\\img\\";

var domain = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?";
var weather_url = domain + "zone=2823768000"; // 부평구 십정동 

function updateWeatherFile(weatherKor, weatherIcon, temp) {
    
    var weatherKorPath = path + "today_weather" + ".txt";
    var weatherIconPath = path + "today_weather_img" + ".txt";
    var weatherTempPath = path + "today_temp" + ".txt";

    var weatherImgLinkPath = pathImg + weatherIcon ;

    fs.writeFileSync(weatherKorPath, weatherKor);
    fs.writeFileSync(weatherIconPath, weatherImgLinkPath);
    fs.writeFileSync(weatherTempPath, temp);

    console.log('File Write Done  : ' + weatherKorPath);
}


var requestOptions = { method: "GET", uri: weather_url, headers: { "User-Agent": "Mozilla/5.0" }};

function weatherIconConvert(weatherKor) {

    var conditions = weatherKor;

    if (weatherKor == '맑음') {
        conditions = 'sunny.png';
    } else if (weatherKor == '구름 조금' ||
    weatherKor == '구름 많음'||
    weatherKor == '흐림') {
        conditions = 'cloudy.png';
    } else  if (weatherKor == '비' ||
    weatherKor == '구름 많고 비'||
    weatherKor == '흐리고 비'||
    weatherKor == '흐리고 비/눈') {
        conditions = 'rain.png';
    } else  if (weatherKor == '눈' ||
    weatherKor == '구름 많고 눈'||
    weatherKor == '흐리고 눈'||
    weatherKor == '흐리고 눈/비') {
        conditions = 'snow.png';
    } else {
        conditions = 'cloudy.png';
     }
    return conditions;
}

function requestAir() {
    request(requestOptions, function(error, response, body) { 
        
        var xmlToJson = convert.xml2json(body, {compact: true, spaces: 4});
    
        var data = JSON.parse(xmlToJson).rss.channel.item.description.body.data[0];

        var weatherKor = data.wfKor._text;
        console.log(weatherKor);

        var weatherTemp = data.temp._text;
        weatherTemp = parseInt(weatherTemp) + '°C';
        console.log(weatherTemp);

        var weatherIcon = weatherIconConvert(weatherKor);

        updateWeatherFile(weatherKor, weatherIcon, weatherTemp);

    }); 
}

requestAir();

setInterval(function(){
    requestAir()
}, 10 * 60 * 1000);
                        
